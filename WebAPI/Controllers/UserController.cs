﻿using BLL;
using Common.DTO.User;
using DAL;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class UserController : ControllerBase
    {
        private readonly IApiUserService _userService;

        public UserController(IApiUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_userService.Get());
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var entity = _userService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public ActionResult Update([FromBody] UpdateUserDTO userDTO)
        {
            _userService.Update(userDTO);
            return Ok();
        }

        [HttpPost]
        public ActionResult Create(NewUserDTO userDTO)
        {          

            return Ok(_userService.Create(userDTO));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _userService.Delete(id);
            return NoContent();
        }
    }
}
