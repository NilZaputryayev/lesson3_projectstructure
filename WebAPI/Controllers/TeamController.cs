﻿using BLL;
using Common.DTO.Team;
using DAL;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class TeamController : ControllerBase
    {
        private readonly IApiTeamService _TeamService;

        public TeamController(IApiTeamService TeamService)
        {
            _TeamService = TeamService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var data = _TeamService.Get();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var entity = _TeamService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public ActionResult Update([FromBody] UpdateTeamDTO TeamDTO)
        {
            _TeamService.Update(TeamDTO);
            return Ok();
        }

        [HttpPost]
        public ActionResult Create(NewTeamDTO TeamDTO)
        {          

            return Ok(_TeamService.Create(TeamDTO));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _TeamService.Delete(id);
            return NoContent();
        }
    }
}
