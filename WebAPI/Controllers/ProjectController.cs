﻿using BLL;
using Common.DTO.Project;
using DAL;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class ProjectController : ControllerBase
    {
        private readonly IApiProjectService _ProjectService;

        public ProjectController(IContext context, IApiProjectService ProjectService)
        {
            _ProjectService = ProjectService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var data = _ProjectService.Get();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var entity = _ProjectService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public ActionResult Update([FromBody] UpdateProjectDTO ProjectDTO)
        {
            _ProjectService.Update(ProjectDTO);
            return Ok();
        }

        [HttpPost]
        public ActionResult Create(NewProjectDTO ProjectDTO)
        {          

            return Ok(_ProjectService.Create(ProjectDTO));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _ProjectService.Delete(id);
            return NoContent();
        }
    }
}
