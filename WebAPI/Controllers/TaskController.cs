﻿using BLL;
using Common.DTO.Task;
using DAL;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class TaskController : ControllerBase
    {
        private readonly IApiTaskService _TaskService;

        public TaskController(IContext context, IApiTaskService TaskService)
        {
            _TaskService = TaskService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var data = _TaskService.Get();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var entity = _TaskService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public ActionResult Update([FromBody] UpdateTaskDTO TaskDTO)
        {
            _TaskService.Update(TaskDTO);
            return Ok();
        }

        [HttpPost]
        public ActionResult Create(NewTaskDTO TaskDTO)
        {          

            return Ok(_TaskService.Create(TaskDTO));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _TaskService.Delete(id);
            return NoContent();
        }
    }
}
