﻿using AutoMapper;
using BLL;
using BLL.Interfaces;
using DAL;
using DAL.Models;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BLL.MappingProfiles;
using Common.Services.ExternalApi;
using Common.Interfaces.ExternalApi;
using System.Collections.Generic;
using Common.ExternalApi.Services;

namespace WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static async System.Threading.Tasks.Task RegisterCustomServicesAsync(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<ITeamService, TeamService>();
            services.AddSingleton<ITaskService, TaskService>();
            services.AddSingleton<IProjectService, ProjectService>();



            services.AddHttpClient("ExternalApiClient", client =>
             {
                 client.BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/");
                 client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
             });

            services.AddHttpClient("InternalApiClient", client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/");
                client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            });

            // services.AddSingleton<IContext,Context>();

            services.AddSingleton<IContext, Context>(serviceProvider =>
            {
                return new Context()
                {
                    Users = GetUsers(services),
                    Teams = GetTeams(services),
                    Tasks = GetTasks(services),
                    Projects = GetProjects(services)
                };
            });


            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<ITaskRepository, TaskRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();


            services.AddTransient<IApiUserService, ApiUserService>();
            services.AddTransient<IApiTeamService, ApiTeamService>();
            services.AddTransient<IApiTaskService, ApiTaskService>();
            services.AddTransient<IApiProjectService, ApiProjectService>();


            //var provider = services.BuildServiceProvider();

            //var dependency = provider.GetRequiredService<IUserService>();

            //var res = await dependency.GetAsync();

            //var context = provider.GetRequiredService<IContext>();



        }

        private static List<User> GetUsers(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();

            var dependency = provider.GetRequiredService<IUserService>();

            var res = dependency.GetAsync();

            return res.Result;
        }

        private static List<Team> GetTeams(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();

            var dependency = provider.GetRequiredService<ITeamService>();

            var res = dependency.GetAsync();

            return res.Result;
        }

        private static List<DAL.Models.Task> GetTasks(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();

            var dependency = provider.GetRequiredService<ITaskService>();

            var res = dependency.GetAsync();

            return res.Result;
        }

        private static List<Project> GetProjects(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();

            var dependency = provider.GetRequiredService<IProjectService>();

            var res = dependency.GetAsync();

            return res.Result;
        }



        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectProfile>();
            },
            Assembly.GetExecutingAssembly());
        }



    }
}
