﻿using AutoMapper;
using Common.DTO.Team;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, UpdateTeamDTO>();
            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<Team, NewTeamDTO>()
                .ReverseMap();
        }
    }
}
