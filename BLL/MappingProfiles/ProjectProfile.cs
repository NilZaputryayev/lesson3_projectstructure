﻿using AutoMapper;
using Common.DTO.Project;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, UpdateProjectDTO>();
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Project, NewProjectDTO>()
                .ReverseMap();
        }
    }
}
