﻿using AutoMapper;
using DAL.Interfaces;

namespace BLL
{
    public abstract class BaseService
    {
        private protected readonly IContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(IContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }

}