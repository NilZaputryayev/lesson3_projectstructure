﻿using Common.DTO.Team;
using DAL.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IApiTeamService
    {
        void Delete(int id);
        List<TeamDTO> Get();
        TeamDTO GetByID(int id);
        void Update(UpdateTeamDTO team);
        TeamDTO Create(NewTeamDTO user);

    }
}