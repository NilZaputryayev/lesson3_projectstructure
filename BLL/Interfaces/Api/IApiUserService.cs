﻿using Common.DTO.User;
using DAL.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IApiUserService
    {
        void Delete(int id);
        List<UserDTO> Get();
        UserDTO GetByID(int id);
        void Update(UpdateUserDTO user);
        UserDTO Create(NewUserDTO user);
    }
}