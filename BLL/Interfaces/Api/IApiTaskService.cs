﻿using Common.DTO.Task;
using DAL.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IApiTaskService
    {
        void Delete(int id);
        List<TaskDTO> Get();
        TaskDTO GetByID(int id);
        void Update(UpdateTaskDTO task);
        TaskDTO Create(NewTaskDTO task);
    }
}