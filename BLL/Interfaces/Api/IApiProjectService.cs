﻿using Common.DTO.Project;
using DAL.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IApiProjectService
    {
        void Delete(int id);
        List<ProjectDTO> Get();
        ProjectDTO GetByID(int id);
        void Update(UpdateProjectDTO project);
        ProjectDTO Create(NewProjectDTO project);
    }
}