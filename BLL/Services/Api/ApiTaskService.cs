﻿using AutoMapper;
using BLL.Interfaces;
using Common.DTO.Task;
using DAL;
using DAL.Interfaces;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace BLL
{
    public class ApiTaskService : BaseService, IApiTaskService
    {
        private ITaskRepository _repo;

        public ApiTaskService(IContext context, IMapper mapper) : base(context, mapper)
        {
            _repo = new TaskRepository(_context);
        }

        public List<TaskDTO> Get()
        {
            return _mapper.Map<List<TaskDTO>>(_repo.GetAll());
        }
        public TaskDTO GetByID(int id)
        {
            return _mapper.Map<TaskDTO>(_repo.GetById(id));
        }
        public void Update(UpdateTaskDTO DTO)
        {
            var usr = _repo.GetById(DTO.id);

            if (usr != null)
            {
                usr.name = DTO.name;
                usr.description = DTO.description;
                usr.finishedAt = DTO.finishedAt;
                usr.performerId = DTO.performerId;

                _repo.Update(usr);

            }
        }
        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public TaskDTO Create(NewTaskDTO entity)
        {
            var model = _mapper.Map<Task>(entity);

            var maxID = _repo.GetAll().Max(x => x.id);

            model.id = maxID + 1;


            return _mapper.Map<TaskDTO>(_repo.Create(model));
        }

    }
}
