﻿using AutoMapper;
using BLL.Interfaces;
using Common.DTO.Team;
using DAL;
using DAL.Interfaces;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ApiTeamService : BaseService, IApiTeamService
    {
        private ITeamRepository _repo;

        public ApiTeamService(IContext context, IMapper mapper) : base(context, mapper)
        {
            _repo = new TeamRepository(_context);
        }

        public List<TeamDTO> Get()
        {
            return _mapper.Map<List<TeamDTO>>(_repo.GetAll());
        }
        public TeamDTO GetByID(int id)
        {
            return _mapper.Map<TeamDTO>(_repo.GetById(id));
        }
        public void Update(UpdateTeamDTO DTO)
        {
            var entity = _repo.GetById(DTO.id);

            if (entity != null)
            {
                entity.name = DTO.name;
                _repo.Update(entity);

            }
        }
        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public TeamDTO Create(NewTeamDTO entity)
        {
            var model = _mapper.Map<Team>(entity);

            var maxID = _repo.GetAll().Max(x => x.id);

            model.id = maxID + 1;


            return _mapper.Map<TeamDTO>(_repo.Create(model));
        }


    }
}
