﻿using AutoMapper;
using Common.DTO.User;
using DAL;
using DAL.Interfaces;
using DAL.Models;

using System.Collections.Generic;
using System.Linq;


namespace BLL
{
    public class ApiUserService : BaseService, IApiUserService
    {
        private IUserRepository _repo;

        public ApiUserService(IContext context, IMapper mapper) : base (context,mapper)
        {
            _repo = new UserRepository(_context);
        }

        public List<UserDTO> Get()
        {
            return _mapper.Map<List<UserDTO>>(_repo.GetAll());
        }
        public UserDTO GetByID(int id)
        {
            return _mapper.Map<UserDTO>(_repo.GetById(id));
        }
        public void Update(UpdateUserDTO DTO)
        {
            var usr = _repo.GetById(DTO.id);

            if(usr != null)
            {
                usr.firstName = DTO.firstName;
                usr.lastName = DTO.lastName;
                usr.email = DTO.email;
                usr.birthDay = DTO.birthDay;

                _repo.Update(usr);

            }
        }
        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public UserDTO Create(NewUserDTO user)
        {
            var userModel = _mapper.Map<User>(user);

            var maxID = _repo.GetAll().Max(x => x.id);

            userModel.id = maxID + 1;


            return _mapper.Map<UserDTO>(_repo.Create(userModel));
        }
    }
}
