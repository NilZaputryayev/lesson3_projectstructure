﻿using AutoMapper;
using BLL.Interfaces;
using Common.DTO.Project;
using DAL;
using DAL.Interfaces;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ApiProjectService : BaseService, IApiProjectService
    {
        private IProjectRepository _repo;

        public ApiProjectService(IContext context, IMapper mapper) : base(context, mapper)
        {
            _repo = new ProjectRepository(_context);
        }

        public List<ProjectDTO> Get()
        {
            return _mapper.Map<List<ProjectDTO>>(_repo.GetAll());
        }
        public ProjectDTO GetByID(int id)
        {
            return _mapper.Map<ProjectDTO>(_repo.GetById(id));
        }
        public void Update(UpdateProjectDTO DTO)
        {
            var entity = _repo.GetById(DTO.id);

            if (entity != null)
            {
                entity.name = DTO.name;
                entity.description = DTO.description;
                entity.authorId = DTO.authorId;
                entity.deadline = DTO.deadline;
                entity.teamId = DTO.teamId;

                _repo.Update(entity);

            }
        }
        public void Delete(int id)
        {
            _repo.Delete(id);
        }

        public ProjectDTO Create(NewProjectDTO entity)
        {
            var model = _mapper.Map<Project>(entity);

            var maxID = _repo.GetAll().Max(x => x.id);

            model.id = maxID + 1;


            return _mapper.Map<ProjectDTO>(_repo.Create(model));
        }


    }
}
