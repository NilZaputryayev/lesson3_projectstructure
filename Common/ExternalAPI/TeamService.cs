﻿using Common.Abstract;
using Common.Interfaces.ExternalApi;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common.ExternalApi.Services
{
    public class TeamService : ExternalBaseService<Team>, ITeamService
    {

        public TeamService(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
            Endpoint = "Teams";
        }


        public override Task<List<Team>> GetAsync()
        {
            return base.GetAsync();
        }
    }
}
