﻿using Common.Abstract;
using Common.Interfaces.ExternalApi;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common.Services.ExternalApi
{
    public class UserService : ExternalBaseService<User>, IUserService
    {

        public UserService(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
            Endpoint = "Users";
        }

        public override Task<List<User>> GetAsync()
        {
            return base.GetAsync();
        }
        
    }
}
