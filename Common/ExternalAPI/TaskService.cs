﻿using Common.Abstract;
using Common.Interfaces.ExternalApi;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace BLL
{
    public class TaskService : ExternalBaseService<Task>, ITaskService
    {

        public TaskService(IHttpClientFactory httpClientFactory) : base(httpClientFactory) {
            Endpoint = "Tasks";
        }

        public override System.Threading.Tasks.Task<List<Task>> GetAsync()
        {
            return base.GetAsync();
        }
    }
}
