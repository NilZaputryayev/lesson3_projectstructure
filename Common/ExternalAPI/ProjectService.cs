﻿using Common.Abstract;
using Common.Interfaces.ExternalApi;
using DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common.ExternalApi.Services
{
    public class ProjectService : ExternalBaseService<Project> ,IProjectService
    {

        public ProjectService(IHttpClientFactory httpClientFactory) : base(httpClientFactory) {
            Endpoint = "Projects";
        }

        public override Task<List<Project>> GetAsync()
        {
            return base.GetAsync();
        }
    }
}
