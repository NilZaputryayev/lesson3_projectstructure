﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common.Abstract
{
    public class ExternalBaseService<T>  where T: class
    {
        private protected HttpClient _client;

        IHttpClientFactory _httpClientFactory;
        public virtual string Endpoint { get; set; }

        private static JsonSerializerSettings jsonSettings;

        protected ExternalBaseService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            ConfigureClient();
        }


        public void ConfigureClient()
        {
            _client = _httpClientFactory.CreateClient("ExternalApiClient");
            jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public virtual async Task<List<T>> GetAsync()
        {
            List<T> data = new List<T>();

            string endpoint = Endpoint;

            try
            {
                var response = await _client.GetAsync(endpoint);

                if (response.IsSuccessStatusCode)


                    data = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync(), jsonSettings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return data;


        }
  
    }
}
