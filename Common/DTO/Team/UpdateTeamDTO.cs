﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTO.Team
{
    public class UpdateTeamDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
