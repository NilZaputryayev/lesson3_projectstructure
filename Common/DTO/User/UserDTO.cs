﻿using System;

namespace Common.DTO.User
{
    public class UserDTO
    {
        public int id { get; set; }
        public int teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName
        {
            get
            {
                return $"{firstName} {lastName}";
            }
        }

        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }

    }
}