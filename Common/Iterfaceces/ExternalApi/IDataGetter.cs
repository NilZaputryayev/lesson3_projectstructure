﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IDataGetter<T> where T : class
    {
        Task<List<T>> Get();
    }
}
