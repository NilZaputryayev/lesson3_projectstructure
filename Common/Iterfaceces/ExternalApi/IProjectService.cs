﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Interfaces.ExternalApi
{
    public interface IProjectService
    {
        Task<List<Project>> GetAsync();
    }
}