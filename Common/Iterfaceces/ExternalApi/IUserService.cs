﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Interfaces.ExternalApi
{
    public interface IUserService
    {
        Task<List<User>> GetAsync();
    }
}