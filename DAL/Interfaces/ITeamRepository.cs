﻿using DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ITeamRepository
    {
        Team GetById(int id);
        IEnumerable<Team> GetAll();
        void Update (Team entity);
        void Delete(int id);
        Team Create(Team entity);
        void Save();


    }
}
