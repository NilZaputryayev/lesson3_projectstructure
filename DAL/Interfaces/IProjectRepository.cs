﻿using DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IProjectRepository
    {
        Project GetById(int id);
        IEnumerable<Project> GetAll();
        void Update (Project entity);
        void Delete(int id);
        Project Create(Project entity);
        void Save();


    }
}
