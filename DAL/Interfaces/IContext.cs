﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Interfaces
{
    public interface IContext
    {
        public List<User> Users { get; set; }
        public List<Team> Teams { get; set; }
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }

        public System.Threading.Tasks.Task SeedData();
  

    }
}
