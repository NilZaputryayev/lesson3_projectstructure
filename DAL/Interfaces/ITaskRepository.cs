﻿using DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Interfaces
{
    public interface ITaskRepository
    {
        Task GetById(int id);
        IEnumerable<Task> GetAll();
        void Update (Task entity);
        void Delete(int id);
        Task Create(Task entity);
        void Save();


    }
}
