﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserRepository : IUserRepository
    {
        private List<User> _dataSet;

        private IContext _context;


        public UserRepository(IContext context)
        {
            _context = context;
            _dataSet = context.Users;
        }

        public User Create(User user)
        {
            
            _dataSet.Add(user);
            Save();
            return user;
        }

        public void Delete(int id)
        {
            var entity = _dataSet.FirstOrDefault(x => x.id == id);

            if (entity != null)
            {
                _dataSet.Remove(entity);
                Save();

            }
        }

        public IEnumerable<User> GetAll()
        {
            return _dataSet;
        }

        public User GetById(int id)
        {
            return _dataSet.FirstOrDefault(x => x.id == id);
        }

        public void Update(User entityToUpdate)
        {
            var entity = _dataSet.FirstOrDefault(x => x.id == entityToUpdate.id);

            if (entity != null)
            {
                entity = entityToUpdate;
                Save();

            }
        }

        public void Save()
        {
            _context.Users = _dataSet;
        }
    }
}
