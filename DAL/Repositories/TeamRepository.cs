﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TeamRepository : ITeamRepository
    {
        private IContext _context;

        private List<Team> _dataSet;
        public TeamRepository(IContext context)
        {
            _context = context;
            _dataSet = context.Teams;
        }
        public void Delete(int id)
        {
            var entity = _dataSet.Find(x => x.id == id);

            if (entity != null)
            {
                _dataSet.Remove(entity);
                Save();
            }
        }

        public IEnumerable<Team> GetAll()
        {
            return _dataSet;
        }

        public Team GetById(int id)
        {
            return _dataSet.FirstOrDefault(x => x.id == id);
        }

        public void Update(Team entityToUpdate)
        {
            var entity = _dataSet.Find(x => x.id == entityToUpdate.id);

            if (entity != null)
            {
                entity = entityToUpdate;
                Save();
            }
        }

        public Team Create(Team entity)
        {
            _dataSet.Add(entity);
            Save();
            return entity;
        }

        public void Save()
        {
            _context.Teams = _dataSet;
        }
    }
}
