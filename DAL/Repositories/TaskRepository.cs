﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class TaskRepository : ITaskRepository
    {
        private IContext _context;

        private List<Task> _dataSet;
        public TaskRepository(IContext context)
        {
            _context = context;
            _dataSet = context.Tasks;
        }
        public void Delete(int id)
        {
            var entity = _dataSet.Find(x => x.id == id);

            if (entity != null)
            {
                _dataSet.Remove(entity);
                Save();
            }
        }

        public IEnumerable<Task> GetAll()
        {
            return _dataSet;
        }

        public Task GetById(int id)
        {
            return _dataSet.FirstOrDefault(x => x.id == id);
        }

        public void Update(Task entityToUpdate)
        {
            var entity = _dataSet.Find(x => x.id == entityToUpdate.id);

            if (entity != null)
            {
                entity = entityToUpdate;
                Save();
            }
        }

        public void Save()
        {
            _context.Tasks = _dataSet;

        }

        public Task Create(Task entity)
        {
            _dataSet.Add(entity);
            Save();
            return entity;
        }

    }
}
