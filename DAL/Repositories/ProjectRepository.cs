﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProjectRepository : IProjectRepository
    {
        private IContext _context;
        private List<Project> _dataSet;
        public ProjectRepository(IContext context)
        {
            _context = context;
            _dataSet = context.Projects;
        }
        public void Delete(int id)
        {
            var entity = _dataSet.Find(x => x.id == id);

            if (entity != null)
            {
                _dataSet.Remove(entity);
                Save();
            }
        }

        public IEnumerable<Project> GetAll()
        {
            return _dataSet;
        }

        public Project GetById(int id)
        {
            return _dataSet.FirstOrDefault(x => x.id == id);
        }

        public void Update(Project entityToUpdate)
        {
            var entity = _dataSet.Find(x => x.id == entityToUpdate.id);

            if (entity != null)
            {
                entity = entityToUpdate;
                Save();
            }
        }
        public void Save()
        {
            _context.Projects = _dataSet;
        }

        public Project Create(Project entity)
        {
            _dataSet.Add(entity);
            Save();
            return entity;
        }
    }
}
