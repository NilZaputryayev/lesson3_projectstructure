﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DAL
{
    public class Context : IContext
    {        
        public List<User> Users { get; set; }
        public List<Team> Teams { get; set; }
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }


        

        public Context()
        { 

            SeedData();

        }


        public System.Threading.Tasks.Task SeedData()
        {
            //var user = new User() { id = 1, firstName = "Igor", lastName = "Kocherga", email = "igtjj@gmail.com", teamId = 1 };
            //List<User> users = new List<User>() { user };

            //var team = new Team { id = 1, name = "One", users = users };
            //var teams = new List<Team>() { team };

           // Teams = new List<Team>();
           // Users = new List<User>();

            

            return System.Threading.Tasks.Task.FromResult("");
        }

        
    }
}
