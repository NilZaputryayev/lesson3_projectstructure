﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{

    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
        public IEnumerable<User> users { get; set; }
    }



}
