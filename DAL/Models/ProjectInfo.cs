﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int CountUsersByCondition { get; set; }

        public void Print()
        {
            Console.WriteLine($"User:\n\r{Project.ToString()}");
            Console.WriteLine($"LongestTaskByDescription:\n\r{LongestTaskByDescription.ToString()}");
            Console.WriteLine($"ShortestTaskByName:\n\r{ShortestTaskByName}");
            Console.WriteLine($"CountUsersByCondition: {CountUsersByCondition}");
        }

    }
}
